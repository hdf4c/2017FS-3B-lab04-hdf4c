#!/bin/bash


choice() {
read input
case $input in
  v) less $file
     ;;
  e) vim $file
     ;;
  c) g++ $file 
     ;;
  q)
     ;;

  *) echo "INVALID RESPONSE
            Skipping this file!"
     ;;

  esac
}


for file in *.txt; do
   echo "
       v) View $file
       e) Edit $file
       c) Compile $file
       q) Quit $file"
   choice
done





