#!/bin/bash

echo "$1: "
if [[ ( -n "$1") && ( -n "$2") ]]; then
  wget -q $2 -O  - | grep -o $1 | wc -l
else 
 echo "Usage goog.sh WORD WEBSITE"

fi

